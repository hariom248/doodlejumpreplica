using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour {

	public Transform platformPrefab;

	public int numberOfPlatforms = 20;
	public float levelWidth = 3f;
	public float minY = .2f;
	public float maxY = 1.5f;

	public Transform initialPlatform;

	public Transform player;

	private Queue<Transform> platforms;

	Vector3 spawnPosition;
	public float recycleOffset;

	void Start () 
	{
		spawnPosition = initialPlatform.position;
		platforms = new Queue<Transform> (numberOfPlatforms+1);
		for (int i = 0; i < numberOfPlatforms; i++)
		{
			platforms.Enqueue (Instantiate (platformPrefab, new Vector3(0,-1000f, spawnPosition.z), Quaternion.identity));
		}
		platforms.Enqueue (initialPlatform);
	}

	void FixedUpdate()
	{
		if(platforms.Peek().localPosition.y + recycleOffset < player.position.y)
		{
			Recycle();
		}
	}

	private void Recycle()
	{
		Transform o = platforms.Dequeue ();
		spawnPosition.y += Random.Range(minY, maxY);
		spawnPosition.x = Random.Range(-levelWidth, levelWidth);
		o.position = spawnPosition;
		platforms.Enqueue (o);
	}
}
