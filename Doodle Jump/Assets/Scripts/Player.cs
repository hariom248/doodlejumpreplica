using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour {

	public float movementSpeed = 10f;

	Rigidbody2D rb;

	float movement = 0f;

	float leftBorder, rightBorder;

	public HUD hud;

	void Start () 
	{
		rb = GetComponent<Rigidbody2D>();
		leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)).x;
		rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0)).x ;
	}
	
	// Update is called once per frame
	void Update () 
	{
		movement = Input.GetAxis("Horizontal") * movementSpeed;
		hud.UpdateScore (rb.position.y);
	}

	void FixedUpdate()
	{
		Vector2 velocity = rb.velocity;
		velocity.x = movement;
		rb.velocity = velocity;
	}

	void LateUpdate()
	{
		if (transform.position.x < leftBorder)
		{
			transform.position = (new Vector2(rightBorder, rb.position.y));
		}

		if (transform.position.x > rightBorder)
		{
			transform.position =(new Vector2(leftBorder, rb.position.y));
		}
	}
}
