using System.Collections;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	public Transform target;

	public Transform killTransform;

	public HUD hud;

	void LateUpdate () 
	{
		if (target.position.y > transform.position.y)
		{
			Vector3 newPos = new Vector3 (transform.position.x, target.position.y, transform.position.z);
			transform.position = newPos;
		}
		else if(target.position.y < killTransform.position.y)
		{
			hud.GameOver ();
		}
	}
}
