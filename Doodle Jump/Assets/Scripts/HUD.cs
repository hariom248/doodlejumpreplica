﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUD : MonoBehaviour 
{
	public Text GameOverAnim;
	public Text scoreText;

	public float score;
	public Transform playerTransform;

	void Start () 
	{
		GameOverAnim.enabled = false;
	}

	public void UpdateScore(float y)
	{
		if (y > score)
		{
			score = y;
			scoreText.text = ((int)(10f * score)).ToString ();
		}
	}

	public void OnPause()
	{
		Time.timeScale = Time.timeScale == 0 ? 1 : 0;
	}

	public void GameOver()
	{
		GameOverAnim.enabled = true;
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}
}
