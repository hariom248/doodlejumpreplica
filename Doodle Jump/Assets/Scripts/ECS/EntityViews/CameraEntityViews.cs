using Svelto.ECS;
using Gameplay.Components.Camera;
using Gameplay.Components.Base;

namespace Gameplay.EntityViews.Camera
{
	public class CameraEntityView : EntityView
	{
		public ICameraComponent         cameraComponent;
		public IConstraintComponent     constraintComponent;
	}

	public class CameraFollowEntityView : EntityView
	{
		public ICameraComponent         cameraComponent;
		public ITransformComponent     	transformComponent;
	}
}