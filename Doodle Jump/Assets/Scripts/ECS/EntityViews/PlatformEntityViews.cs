﻿using Svelto.ECS;
using Gameplay.Components.Platforms;

namespace Gameplay.EntityViews.Platforms
{
	public class PlatformEntityView : EntityView
	{
		public ICollisionEnter         	collisionEnterComponent;
	}

	public class PlatformSpawnerEntityView : EntityView
	{
		public IPlatformSpawner 		platformSpawnerComponent;
	}
}