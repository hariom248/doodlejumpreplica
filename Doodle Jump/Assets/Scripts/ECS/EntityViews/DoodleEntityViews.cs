﻿using Svelto.ECS;
using Gameplay.Components.Doodle;

namespace Gameplay.EntityViews.Doodle
{
	public class DoodleEntityView : EntityView
	{
		public ISpeedComponent         	speedComponent;
		public IRigidBodyComponent     	rigidBodyComponent;
		public IPositionComponent      	positionComponent;
		public IJumpComponent			jumpComponent;
	}
}