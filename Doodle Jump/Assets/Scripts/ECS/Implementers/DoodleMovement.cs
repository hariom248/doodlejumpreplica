﻿using UnityEngine;
using Gameplay.Components.Doodle;
using Gameplay;

namespace Gameplay.Implementors.Doodle
{
	public class DoodleMovement : MonoBehaviour, IImplementor, ISpeedComponent, IRigidBodyComponent, IPositionComponent, IJumpComponent 
	{
		public float movementSpeed = 10f;

		float ISpeedComponent.speed { get { return movementSpeed; }}
		Rigidbody2D IRigidBodyComponent.rigidbody { get { return rigidBody; }}
		Vector3 IPositionComponent.position { get { return playerTranform.position; }}
		float IJumpComponent.jumpForce { get { return jumpForce; } set { jumpForce = value; }}

		void Awake () 
		{
			this.playerTranform = transform;
			this.rigidBody 		= GetComponent<Rigidbody2D> ();
		}

		Rigidbody2D rigidBody;
		Transform playerTranform;
		float jumpForce;
	}
}