﻿using UnityEngine;
using Gameplay.Components.Platforms;
using System.Collections.Generic;

namespace Gameplay.Implementors.PlatformSpawner
{
	public class PlatformSpawner : MonoBehaviour, IImplementor, IPlatformSpawner
	{
		int IPlatformSpawner.numberOfPlatforms 		{get { return NumberOfPlatforms;}}
		float IPlatformSpawner.maxWidth 			{get { return MaxWidth;}}
		float IPlatformSpawner.minY 				{get { return MinY;}}
		float IPlatformSpawner.maxY 				{get { return MaxY;}}
		float IPlatformSpawner.recycleOffset 		{get { return RecycleOffset;}}
		Queue<Transform> IPlatformSpawner.platforms {get; set;}
		Vector3 IPlatformSpawner.spawnPos			{get; set;}
		GameObject IPlatformSpawner.platformPrefab  {get { return PlatformPrefab;}}

		public int NumberOfPlatforms 	= 15;
		public float MaxWidth			= 3f;
		public float MinY 				= .2f;
		public float MaxY 				= 1.5f;
		public float RecycleOffset		= 5f;
		public GameObject PlatformPrefab;
	}
}