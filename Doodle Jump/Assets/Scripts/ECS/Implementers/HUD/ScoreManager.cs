﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay.Components.HUD;
using UnityEngine.UI;
using Gameplay;

namespace Gameplay.Implementors.HUD.Score
{
	public class ScoreManager : MonoBehaviour, IImplementor, IScoreComponent
	{
		int IScoreComponent.score  {get { return _score;} set {_score = value; _text.text = scoreText + _score;}}
		int IScoreComponent.multiplier {get { return _multiplier;}}

		void Awake()
		{
			_text = GetComponent<Text> ();
		}

		int _multiplier = 10;
		int _score;
		Text _text;
		const string scoreText = "Score : ";
	}
}