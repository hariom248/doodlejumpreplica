﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay;
using Gameplay.Components.Platforms;

namespace Gameplay.Implementors.Platforms
{
	public class Platform : MonoBehaviour, IImplementor, ICollisionEnter
	{
		float ICollisionEnter.jumpforce { get { return JumpForce;}}
		public event System.Action<int, int, float> collisionEnter;

		public float JumpForce = 10f;

		void OnCollisionEnter2D(Collision2D collision)
		{
			if (collisionEnter != null)
				collisionEnter(collision.gameObject.GetInstanceID(), gameObject.GetInstanceID(), collision.relativeVelocity.y);
		}
	}
}