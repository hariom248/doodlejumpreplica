﻿using UnityEngine;
using Gameplay.Components.Camera;
using Gameplay.Components.Base;
using Gameplay;

namespace Gameplay.Implementors.Cameras
{
	public class GameCamera : MonoBehaviour, IImplementor, ICameraComponent, IConstraintComponent, ITransformComponent 
	{
		Camera ICameraComponent.camera { get { return _camera; }}
		float IConstraintComponent.leftMargin { get { return _leftMargin; }}
		float IConstraintComponent.rightMargin { get { return _rightMargin; }}
		float IConstraintComponent.bottomMargin { get; set;}
		Transform ITransformComponent.transform	{get { return _transform;}}

		void Awake () 
		{
			_camera 		= GetComponent<Camera>();
			_leftMargin 	= _camera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x;
			_rightMargin 	= _camera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x;
			_transform		= transform;
		}

		Camera _camera;
		float _leftMargin;
		float _rightMargin;
		Transform _transform;
	}
}