﻿using Svelto.ECS;
using Gameplay.EntityViews.Camera;
using UnityEngine;
using Gameplay.EntityViews.Doodle;
using System.Collections;
using Svelto.Tasks;

namespace Gameplay.Engines.Camera
{
	public class CameraFollowEngine : MultiEntityViewsEngine<DoodleEntityView, CameraFollowEntityView>
	{
		public CameraFollowEngine()
		{
			PhysicsTick ().RunOnSchedule (StandardSchedulers.physicScheduler);
		}

		protected override void Add (DoodleEntityView entityView)
		{
			_doodleEntityView = entityView;
		}

		protected override void Remove (DoodleEntityView entityView)
		{
			_doodleEntityView = null;
		}

		protected override void Add (CameraFollowEntityView node)
		{
			_cameraFollowEntityView = node;
		}
		protected override void Remove (CameraFollowEntityView node)
		{
			_cameraFollowEntityView = null;
		}

		IEnumerator PhysicsTick()
		{
			while (true)
			{
				if (_doodleEntityView != null && _cameraFollowEntityView != null)
				{
					float doodleYPos 	= _doodleEntityView.positionComponent.position.y;
					float cameraYPos 	= _cameraFollowEntityView.transformComponent.transform.position.y;
					if (doodleYPos > cameraYPos)
					{
						Vector3 newPos = _cameraFollowEntityView.transformComponent.transform.position;
						newPos.y = doodleYPos;
						_cameraFollowEntityView.transformComponent.transform.position = newPos;
					}
				}
				yield return new WaitForFixedUpdate ();
			}
		}

		CameraFollowEntityView _cameraFollowEntityView;
		DoodleEntityView _doodleEntityView;
	}
}