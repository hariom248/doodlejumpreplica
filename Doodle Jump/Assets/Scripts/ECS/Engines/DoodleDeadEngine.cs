using Svelto.ECS;
using Svelto.Tasks;
using Gameplay.EntityViews.Camera;
using Gameplay.EntityViews.Doodle;
using System.Collections;
using UnityEngine;

namespace Gameplay.Engines.Camera
{
	public class DoodleDeadEngine : MultiEntityViewsEngine<DoodleEntityView, CameraEntityView>
	{
		public DoodleDeadEngine()
		{
			
		}

		protected override void Add (DoodleEntityView entityView)
		{
			_doodleView = entityView;
			PhysicsTick ().RunOnSchedule (StandardSchedulers.physicScheduler);
		}

		protected override void Remove (DoodleEntityView entityView)
		{
			_doodleView = null;
		}

		protected override void Add (CameraEntityView node)
		{
			_cameraNode = node;
		}
		protected override void Remove (CameraEntityView node)
		{
			_cameraNode = null;
		}

		IEnumerator PhysicsTick()
		{
			while (true)
			{
				_cameraNode.constraintComponent.bottomMargin = _cameraNode.cameraComponent.camera.ViewportToWorldPoint (new Vector3(0,0,0)).y;
				if (_doodleView.positionComponent.position.y < _cameraNode.constraintComponent.bottomMargin)
				{
					Debug.Log ("gameIver");
				}
				yield return new WaitForFixedUpdate ();
			}
		}

		CameraEntityView _cameraNode;
		DoodleEntityView _doodleView;
	}
}
