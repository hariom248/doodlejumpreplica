﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Svelto.ECS;
using System;
using Svelto.Tasks;
using Svelto.Context;

namespace Gameplay.Engines.PlayerInput
{
	public class PlayerInputEngine : IEngine, IWaitForFrameworkInitialization 
	{
		public PlayerInputEngine(Sequencer seq)
		{
			this._sequence = seq;
		}

		IEnumerator PhysicsTick()
		{
			while (true)
			{
				float moveVal = Input.GetAxisRaw ("Horizontal");
				_sequence.Next (this, ref moveVal);

				yield return null;
			}
		}

		public void OnFrameworkInitialized ()
		{
			PhysicsTick ().RunOnSchedule (StandardSchedulers.physicScheduler);
		}

		Sequencer			_sequence;
	}
}