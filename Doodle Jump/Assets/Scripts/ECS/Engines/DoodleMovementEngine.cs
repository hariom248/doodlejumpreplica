﻿using Svelto.ECS;
using Gameplay.EntityViews.Doodle;
using UnityEngine;
using System.Collections;
using Svelto.Tasks;

namespace Gameplay.Engines.Doodle
{
	public class DoodleMovementEngine : SingleEntityViewEngine<DoodleEntityView>, IStep<float> 
	{
		public DoodleMovementEngine(Sequencer seq)
		{
			this._sequence = seq;
		}

		public void Step (ref float token, System.Enum condition)
		{
			if (_doodleNode == null) return;
			Move (token);
		}

		private void Move(float val)
		{
			Rigidbody2D rb 		= _doodleNode.rigidBodyComponent.rigidbody;
			Vector2 velocity 	= rb.velocity;
			velocity.x 			= val * _doodleNode.speedComponent.speed;
			rb.velocity 		= velocity;
			_sequence.Next (this, ref rb);
		}

		protected override void Add (DoodleEntityView node)
		{
			_doodleNode = node;
		}
		protected override void Remove (DoodleEntityView node)
		{
			_doodleNode = null;
		}

		DoodleEntityView		_doodleNode;
		Sequencer		_sequence;
	}
}