﻿using Svelto.ECS;
using Svelto.Factories;
using Svelto.Tasks.Enumerators;
using System.Collections;
using System.IO;
using UnityEngine;
using Gameplay.EntityViews.Platforms;
using Gameplay.EntityDescriptors.Platform;
using Gameplay.Components.Platforms;
using Gameplay.Components;
using Svelto.Tasks;
using System.Collections.Generic;
using Gameplay.EntityViews.Doodle;

public class PlatformSpawnerEngine : MultiEntityViewsEngine<DoodleEntityView, PlatformSpawnerEntityView>
{
	public PlatformSpawnerEngine (IGameObjectFactory gameobjectFactory, IEntityFactory entityFactory)
	{
		_gameobjectFactory = gameobjectFactory;
		_entityFactory = entityFactory;
	}

	protected override void Add (DoodleEntityView entityView)
	{
		_doodleEntityView = entityView;
	}

	protected override void Remove (DoodleEntityView entityView)
	{
		_doodleEntityView = null;
	}

	protected override void Add (PlatformSpawnerEntityView entityView)
	{
		_platformSpawnerEntityView = entityView;
		CreatePlatforms ();
		_platformSpawnerEntityView.platformSpawnerComponent.spawnPos = _doodleEntityView.positionComponent.position;
		PhysicsTick ().RunOnSchedule (StandardSchedulers.physicScheduler);
	}

	protected override void Remove (PlatformSpawnerEntityView entityView)
	{
		_platformSpawnerEntityView = null;
	}

	private void CreatePlatforms()
	{
		_platformSpawnerEntityView.platformSpawnerComponent.platforms = new Queue<Transform> (_platformSpawnerEntityView.platformSpawnerComponent.numberOfPlatforms);
		for (int i = 0; i < _platformSpawnerEntityView.platformSpawnerComponent.numberOfPlatforms; i++)
		{
			var go = _gameobjectFactory.Build (_platformSpawnerEntityView.platformSpawnerComponent.platformPrefab);
			_entityFactory.BuildEntity<PlatformEntityDescriptor> (go.GetInstanceID (), go.GetComponentsInChildren<IComponent> ());
			go.transform.position = new Vector3 (0, -1000f, _platformSpawnerEntityView.platformSpawnerComponent.spawnPos.z);
			_platformSpawnerEntityView.platformSpawnerComponent.platforms.Enqueue (go.transform);
		}
	}

	private void Recycle()
	{
		if(_platformSpawnerEntityView.platformSpawnerComponent.platforms.Peek().localPosition.y + _platformSpawnerEntityView.platformSpawnerComponent.recycleOffset < _doodleEntityView.positionComponent.position.y)
		{
			Transform o = _platformSpawnerEntityView.platformSpawnerComponent.platforms.Dequeue ();
			Vector3 spawnPos = _platformSpawnerEntityView.platformSpawnerComponent.spawnPos;
			spawnPos.y += Random.Range(_platformSpawnerEntityView.platformSpawnerComponent.minY, _platformSpawnerEntityView.platformSpawnerComponent.maxY);
			spawnPos.x = Random.Range(-_platformSpawnerEntityView.platformSpawnerComponent.maxWidth, _platformSpawnerEntityView.platformSpawnerComponent.maxWidth);
			o.position = spawnPos;
			_platformSpawnerEntityView.platformSpawnerComponent.spawnPos = spawnPos;
			_platformSpawnerEntityView.platformSpawnerComponent.platforms.Enqueue (o);
		}
	}

	IEnumerator PhysicsTick()
	{
		while (true)
		{
			Recycle ();
			yield return new WaitForFixedUpdate ();
		}
	}

	DoodleEntityView					_doodleEntityView;
	PlatformSpawnerEntityView			_platformSpawnerEntityView;
	Svelto.Factories.IGameObjectFactory _gameobjectFactory;
	IEntityFactory                      _entityFactory;
}
