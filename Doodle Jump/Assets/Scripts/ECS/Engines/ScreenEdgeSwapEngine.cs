﻿using Svelto.ECS;
using Gameplay.EntityViews.Camera;
using UnityEngine;

namespace Gameplay.Engines.Camera
{
	public class ScreenEdgeSwapEngine : SingleEntityViewEngine<CameraEntityView>, IStep<Rigidbody2D> 
	{
		public ScreenEdgeSwapEngine()
		{
		}

		protected override void Add (CameraEntityView node)
		{
			_cameraNode = node;
		}
		protected override void Remove (CameraEntityView node)
		{
			_cameraNode = null;
		}

		public void Step (ref Rigidbody2D token, System.Enum condition)
		{
			if (_cameraNode == null) return;
			Vector3 pos = token.position;
			if (pos.x < _cameraNode.constraintComponent.leftMargin)
			{
				token.position =  (new Vector2(_cameraNode.constraintComponent.rightMargin, pos.y));
			}

			if (pos.x > _cameraNode.constraintComponent.rightMargin)
			{
				token.position =  (new Vector2(_cameraNode.constraintComponent.leftMargin, pos.y));
			}
		}

		CameraEntityView _cameraNode;
	}
}
