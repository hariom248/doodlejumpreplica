﻿using Svelto.ECS;
using Gameplay.EntityViews.Doodle;
using UnityEngine;
using System.Collections;
using Svelto.Tasks;
using Gameplay.EntityViews.Platforms;

namespace Gameplay.Engines.Doodle
{
	public class DoodleJumpEngine : MultiEntityViewsEngine<PlatformEntityView, DoodleEntityView>, IQueryingEntityViewEngine
	{
		public IEntityViewsDB entityViewsDB { set; private get; }

		public DoodleJumpEngine(Sequencer seq)
		{
			this._sequence = seq;
		}

		public void Ready ()
		{
			
		}

		private void Jump(float val)
		{
			Rigidbody2D rb 		= _doodleNode.rigidBodyComponent.rigidbody;
			Vector2 velocity 	= rb.velocity;
			velocity.y 			= val;
			rb.velocity 		= velocity;
			float yPos 			= rb.position.y;
			_sequence.Next (this, ref yPos);
		}

		protected override void Add(PlatformEntityView obj)
		{
			PlatformEntityView enemyEntityView = (obj as PlatformEntityView);

			enemyEntityView.collisionEnterComponent.collisionEnter += CheckTarget;
		}

		protected override void Remove(PlatformEntityView obj)
		{
			PlatformEntityView enemyEntityView = (obj as PlatformEntityView);

			enemyEntityView.collisionEnterComponent.collisionEnter -= CheckTarget;
		}

		protected override void Add(DoodleEntityView obj)
		{
			_doodleNode = obj as DoodleEntityView;
		}

		protected override void Remove(DoodleEntityView obj)
		{
			_doodleNode = null;
		}

		void CheckTarget(int targetID, int enemyID, float relativeYSpeed)
		{
			if (_doodleNode == null)
				return;

			if (targetID == _doodleNode.ID)
			{
				var enemyEntityView = entityViewsDB.QueryEntityView<PlatformEntityView>(enemyID);
				var component = enemyEntityView.collisionEnterComponent;

				if (relativeYSpeed <= 0f)
				{
					Jump (component.jumpforce);
				}
			}
		}

		DoodleEntityView		_doodleNode;
		Sequencer				_sequence;
	}
}