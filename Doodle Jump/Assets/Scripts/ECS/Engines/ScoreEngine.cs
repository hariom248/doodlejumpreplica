﻿using UnityEngine;
using Gameplay.EntityViews.HUD;
using Svelto.ECS;

namespace Gameplay.Engines.HUD.Score
{
	public class ScoreEngine : SingleEntityViewEngine<HUDEntityView>, IStep<float>
	{
		public ScoreEngine()
		{
		}

		protected override void Add (HUDEntityView node)
		{
			_hudNode = node;
		}
		protected override void Remove (HUDEntityView node)
		{
			_hudNode = null;
		}

		public void Step (ref float token, System.Enum condition)
		{
			if (_hudNode == null) return;
			float scoreValue = token * _hudNode.scoreComponent.multiplier;
			if (scoreValue > _hudNode.scoreComponent.score)
			{
				_hudNode.scoreComponent.score = (int)scoreValue;
			}
		}

		HUDEntityView _hudNode;
	}

}