﻿using UnityEngine;

namespace Gameplay.Components.HUD
{
	public interface IScoreComponent: IComponent
	{
		int score { set; get; }
		int multiplier { get;}
	}
}