﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace Gameplay.Components.Platforms
{
	public interface ICollisionEnter : IComponent
	{
		event Action<int, int, float> collisionEnter;
		float jumpforce { get;}
	}

	[Serializable]
	public class PlatformSpawnData
	{
		public GameObject platformPrefab;
	}

	public interface IPlatformSpawner : IComponent
	{
		int numberOfPlatforms 			{ get;}
		float maxWidth 					{ get;}
		float minY 						{ get;}
		float maxY 						{ get;}
		float recycleOffset				{ get;}
		GameObject platformPrefab		{ get;}
		Vector3 spawnPos				{ get; set;}
		Queue<Transform> platforms		{ get; set;}
	}
}