﻿using Gameplay.Components;
using UnityEngine;

namespace Gameplay.Components.Base
{
	public interface ITransformComponent: IComponent
	{
		Transform transform { get; }
	}
}