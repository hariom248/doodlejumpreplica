using UnityEngine;

namespace Gameplay.Components.Doodle
{
    public interface IPositionComponent: IComponent
    {
        Vector3 position { get; }
    }

    public interface IRigidBodyComponent: IComponent
    {
        Rigidbody2D rigidbody { get; }
    }

    public interface ISpeedComponent: IComponent
    {
        float speed { get; }
    }

	public interface IJumpComponent: IComponent
	{
		float jumpForce { get; set;}
	}
}