namespace Gameplay.Components.Camera
{
	public interface ICameraComponent : IComponent
	{
		UnityEngine.Camera camera { get;}
	}

	public interface IConstraintComponent : IComponent
	{
		float leftMargin { get;}
		float rightMargin { get;}
		float bottomMargin { get; set;}
	}
}