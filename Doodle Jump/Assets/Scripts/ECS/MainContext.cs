using UnityEngine;
using System.Collections.Generic;
using Steps = System.Collections.Generic.Dictionary<Svelto.ECS.IEngine, System.Collections.Generic.Dictionary<System.Enum, Svelto.ECS.IStep[]>>;
using Svelto.Context;
using Svelto.ECS.Schedulers.Unity;
using Svelto.ECS;
using Gameplay.Engines.Doodle;
using Gameplay.Engines.Camera;
using Gameplay.Engines.PlayerInput;
using Gameplay.Engines.HUD.Score;
using Gameplay;

namespace Gameplay
{
    public class Main : ICompositionRoot
    {
        public Main()
        {
			_contextNotifier = new ContextNotifier();
            SetupEnginesAndComponents();
        }

        void SetupEnginesAndComponents()
        {
			_enginesRoot 				= new EnginesRoot(new UnitySumbmissionEntityViewScheduler());
			_entityFactory 				= _enginesRoot.GenerateEntityFactory ();

			GameObjectFactory factory 	= new GameObjectFactory();

			Sequencer doodleMovementSequence 	= new Sequencer();
			Sequencer playerInputSequence 		= new Sequencer();
			Sequencer doodleJumpSequence 		= new Sequencer ();

			var doodleMovementEngine 	= new DoodleMovementEngine (doodleMovementSequence);
			var screenEdgeSwapEngine 	= new ScreenEdgeSwapEngine ();
			var doodleDeadEngine 		= new DoodleDeadEngine ();
			var playerInputEngine 		= new PlayerInputEngine (playerInputSequence);
			var scoreEngine 			= new ScoreEngine ();
			var platformSpawnerEngine 	= new PlatformSpawnerEngine (factory, _entityFactory);
			var doodleJumpEngine 		= new DoodleJumpEngine (doodleJumpSequence);

			playerInputSequence.SetSequence
			(
				new Steps () 
				{ 
					{ 
						playerInputEngine, 
						new Dictionary<System.Enum, IStep[]>()
						{ 
							{Condition.always, new [] { doodleMovementEngine} }
						}
					}
				}
			);

			doodleMovementSequence.SetSequence
			(
				new Steps () { 
				{ 
					doodleMovementEngine, 
					new Dictionary<System.Enum, IStep[]>()
					{ 
						{ Condition.always, new [] { screenEdgeSwapEngine } },
					}  
				}
			});

			doodleJumpSequence.SetSequence
			(
				new Steps () { 
				{ 
					doodleJumpEngine, 
					new Dictionary<System.Enum, IStep[]>()
					{ 
						{  Condition.always, new [] { scoreEngine }  }
					}  
				}
			});
          
			_contextNotifier.AddFrameworkInitializationListener(playerInputEngine);
			AddEngine (platformSpawnerEngine);
			AddEngine (playerInputEngine);
			AddEngine (doodleMovementEngine);
			AddEngine (doodleJumpEngine);
			AddEngine (screenEdgeSwapEngine);
			AddEngine (new CameraFollowEngine ());
			AddEngine (doodleDeadEngine);
			AddEngine (scoreEngine);
        }

        void AddEngine(IEngine engine)
        {
            _enginesRoot.AddEngine(engine);
        }

        void ICompositionRoot.OnContextCreated(UnityContext contextHolder)
        {
			IEntityDescriptorHolder[] entities = contextHolder.GetComponentsInChildren<IEntityDescriptorHolder>();

			for (int i = 0; i < entities.Length; i++)
			{
				var entityDescriptorHolder = entities[i];
				var entityDescriptor = entityDescriptorHolder.RetrieveDescriptor();
				_entityFactory.BuildEntity (((MonoBehaviour) entityDescriptorHolder).gameObject.GetInstanceID(), 
					entityDescriptor,
					(entityDescriptorHolder as MonoBehaviour).GetComponentsInChildren<IImplementor>());
			}
        }

        void ICompositionRoot.OnContextInitialized()
        {
			_contextNotifier.NotifyFrameworkInitialized();
		}

        void ICompositionRoot.OnContextDestroyed()
        {
			_contextNotifier.NotifyFrameworkDeinitialized();

			_enginesRoot.Dispose();

			TaskRunner.Instance.StopAndCleanupAllDefaultSchedulerTasks();
		}

        EnginesRoot _enginesRoot;
        IEntityFactory _entityFactory;
		ContextNotifier _contextNotifier;
    }

    //A GameObject containing UnityContext must be present in the scene
    //All the monobehaviours present in the scene statically that need
    //to notify the Context, must belong to GameObjects children of UnityContext.

    public class MainContext : UnityContext<Main>
    { }

}