﻿using Gameplay.EntityViews.Platforms;
using Svelto.ECS;
using UnityEngine;
using Gameplay.Components;

namespace Gameplay.EntityDescriptors.Platform
{
	public class PlatformEntityDescriptor : GenericEntityDescriptor<PlatformEntityView>
	{}

	[DisallowMultipleComponent]
	public class PlatformEntityDescriptorHolder : GenericEntityDescriptorHolder<PlatformEntityDescriptor>
	{}
}
