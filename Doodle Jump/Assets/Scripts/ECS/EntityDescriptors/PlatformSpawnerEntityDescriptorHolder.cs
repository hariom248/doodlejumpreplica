﻿using Gameplay.EntityViews.Platforms;
using Svelto.ECS;
using UnityEngine;
using Gameplay.Components;

namespace Gameplay.EntityDescriptors.PlatformSpawner
{
	public class PlatformSpawnerEntityDescriptor : GenericEntityDescriptor<PlatformSpawnerEntityView>
	{}

	[DisallowMultipleComponent]
	public class PlatformSpawnerEntityDescriptorHolder : GenericEntityDescriptorHolder<PlatformSpawnerEntityDescriptor>
	{}
}
