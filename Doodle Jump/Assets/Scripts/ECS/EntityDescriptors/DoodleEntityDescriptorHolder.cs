﻿using Gameplay.EntityViews.Doodle;
using Svelto.ECS;
using UnityEngine;
using Gameplay.Components;

namespace Gameplay.EntityDescriptors.Doodle
{
	public class DoodleEntityDescriptor : GenericEntityDescriptor<DoodleEntityView>
	{}

	[DisallowMultipleComponent]
	public class DoodleEntityDescriptorHolder : GenericEntityDescriptorHolder<DoodleEntityDescriptor>
	{}
}
