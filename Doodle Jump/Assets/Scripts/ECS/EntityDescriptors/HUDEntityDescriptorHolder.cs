﻿using Gameplay.EntityViews.HUD;
using Svelto.ECS;
using UnityEngine;
using Gameplay.Components;

namespace Gameplay.EntityDescriptors.HUD
{
	public class HUDEntityDescriptor : GenericEntityDescriptor<HUDEntityView>
	{}

	[DisallowMultipleComponent]
	public class HUDEntityDescriptorHolder : GenericEntityDescriptorHolder<HUDEntityDescriptor>
	{}
}
