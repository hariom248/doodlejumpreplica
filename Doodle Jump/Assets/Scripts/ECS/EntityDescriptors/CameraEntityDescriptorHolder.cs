﻿using Gameplay.EntityViews.Camera;
using Svelto.ECS;
using UnityEngine;
using Gameplay.Components;

namespace Gameplay.EntityDescriptors.Camera
{
	public class CameraEntityDescriptor : GenericEntityDescriptor<CameraEntityView, CameraFollowEntityView>
	{}

	[DisallowMultipleComponent]
	public class CameraEntityDescriptorHolder : GenericEntityDescriptorHolder<CameraEntityDescriptor>
	{}
}
